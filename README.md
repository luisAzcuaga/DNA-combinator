# ADN Combinator 9000

First of all, sorry about the name, might change it over time. It's just that I didn't know what to call it.

## What's this for?

Ok, little backsotry. A couple of PhD guys at my college asked me for an app that could generate all the possible combinations of a certain DNA chain. As they would normally iterate among all the possible combinations by HAND!
So, this little program I wrote simply reads letter by letter of a DNA chain and finds any letter different than Adenine, Cytosine, Gunanine or Thymine (Uracil) and turns them into one of those 4 according to this -> http://www.bioinformatics.org/sms/iupac.html

## What about the UI

As you can tell I'm not any scientist so I don't have idea about DNA's but I at least tried to leave the UI pretty clean. It's pretty simple to use, still, If you have any doubt hope you reach me, somehow, I'm new to git aswell.