package proyecto;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import javax.swing.JPanel;



public class Backend
extends JPanel
{
	private static final long serialVersionUID = 2L;
	int comodin = 0;
	boolean checado = false;
	int threshold = 0;
	float aprovado = 0.0F;

	int iteracion = 0;

	public float analisis(String cadenaCruda)
	{
		if (!this.checado) {
			for (int n = 0; n < cadenaCruda.length(); n++) {
				char analisis = cadenaCruda.charAt(n);
				if (((analisis == 'R' ? 1 : 0) | (analisis == 'Y' ? 1 : 0) | (analisis == 'S' ? 1 : 0) | (analisis == 'W' ? 1 : 0) | (analisis == 'K' ? 1 : 0) | (analisis == 'M' ? 1 : 0) | (analisis == 'B' ? 1 : 0) | (analisis == 'D' ? 1 : 0) | (analisis == 'H' ? 1 : 0) | (analisis == 'V' ? 1 : 0) | (analisis == 'N' ? 1 : 0)) != 0) {
					this.comodin += 1;
				}
			}
			this.checado = true;

			this.aprovado = ((float)(this.comodin * 1.0D / cadenaCruda.length() * 100.0D));
		}
		return this.aprovado;
	}

	public String arboles(String cadenaOriginal)
	{
		String cadenaMayus = cadenaOriginal.toUpperCase();
		analisis(cadenaMayus);

		if (this.aprovado <= this.threshold) {
			for (int i = 0; i < cadenaMayus.length(); i++) {
				char caracter = cadenaMayus.charAt(i);
				if (caracter == 'R') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'Y') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'S') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'W') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'K') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'M') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'B') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'D') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'H') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'V') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString());
				}
				if (caracter == 'N') {
					return arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("A").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("C").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("G").append(cadenaMayus.substring(i + 1)).toString()) + "\n" + arboles(new StringBuilder(String.valueOf(cadenaMayus.substring(0, i))).append("T").append(cadenaMayus.substring(i + 1)).toString());
				}
			}
			this.iteracion += 1;
			return "[" + this.iteracion + "]\r\n" + cadenaMayus + "\r\n";
		}
		DecimalFormat numberFormat = new DecimalFormat("#.0");
		JOptionPane.showMessageDialog(getParent(), "El threshold est� fijo en " + this.threshold + "%\n�Y se detect� " + numberFormat.format(this.aprovado) + "% de letras comod�n!", "�El codigo es muy impreciso! :(", 1);
		return cadenaOriginal;
	}
}
