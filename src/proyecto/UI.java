package proyecto;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

public class UI extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField directorio;
	private JTextArea codigo;
	String filename;
	Backend brujeria_arcana = new Backend();
	private JSpinner porcentajeImpureza;
	String code = "";
	String ultimoEstado = "";
	String ultimoAnalisis = "";

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI frame = new UI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public UI() throws IOException {
		setIconImage(Toolkit.getDefaultToolkit().getImage(UI.class.getResource("/proyecto/icono.png")));

		setTitle("ADN Combinator 9000");
		setDefaultCloseOperation(3);
		setBounds(100, 100, 500, 450);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setResizable(false);
		setContentPane(this.contentPane);

		final JPanel panel = new JPanel();
		this.contentPane.add(panel, "Center");
		panel.setLayout(null);
		this.directorio = new JTextField();
		this.directorio.setFont(new Font("Calibri", 0, 11));
		this.directorio.setBounds(10, 350, 287, 20);
		panel.add(this.directorio);
		this.directorio.setColumns(10);


		JButton btnNewButton = new JButton("Examinar");
		btnNewButton.setFont(new Font("Calibri", 0, 11));
		btnNewButton.setBounds(10, 377, 85, 23);
		panel.add(btnNewButton);


		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UI.this.code = UI.this.codigo.getText();
				if ((UI.this.code.isEmpty()) || (UI.this.codigo.getText().equals(UI.this.ultimoAnalisis))) {
					UI.this.directorio.setText("�No hay nada que analizar!");
				} else {
					UI.this.brujeria_arcana.checado = false;
					UI.this.brujeria_arcana.comodin = 0;
					UI.this.brujeria_arcana.iteracion = 0;
					UI.this.brujeria_arcana.threshold = ((Integer)UI.this.porcentajeImpureza.getValue()).intValue();
					UI.this.codigo.setText(UI.this.brujeria_arcana.arboles(UI.this.codigo.getText()));
					UI.this.ultimoAnalisis = UI.this.codigo.getText();
				}

			}
		});
		btnIniciar.setFont(new Font("Calibri", 0, 11));
		btnIniciar.setBounds(307, 350, 71, 50);
		panel.add(btnIniciar);
		JButton btnAbout = new JButton("About");
		btnAbout.setFont(new Font("Calibri", 0, 11));
		btnAbout.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) {
				JLabel infoacercade = new JLabel("<html>Desarrollado por <b>Luis Azcuaga</b><br>Tienes la versi�n 1.0<br>�Dudas? �Sugerencias?<br>Escr�beme a luis.azcuaga@gmail.com</html>");
				infoacercade.setFont(new Font("Calibri", 0, 12));
				JOptionPane MsjAbout = new JOptionPane(infoacercade);
				JDialog VentanaAbout = MsjAbout.createDialog(null, "About");
				VentanaAbout.setLocationRelativeTo(panel);
				VentanaAbout.setVisible(true);
			}
		});
		btnAbout.setBounds(416, 0, 65, 14);
		panel.add(btnAbout);

		this.codigo = new JTextArea();
		JScrollPane scroll = new JScrollPane(this.codigo);
		this.codigo.setFont(new Font("Calibri", 0, 12));
		scroll.setBounds(0, 20, 485, 325);
		panel.add(scroll);

		this.porcentajeImpureza = new JSpinner();
		this.porcentajeImpureza.setFont(new Font("Calibri", 0, 11));
		this.porcentajeImpureza.setModel(new SpinnerNumberModel(5, 0, 20, 1));
		this.porcentajeImpureza.setBounds(258, 380, 39, 20);
		panel.add(this.porcentajeImpureza);

		JLabel lblUmbral = new JLabel("Umbral");
		lblUmbral.setFont(new Font("Calibri", 0, 11));
		lblUmbral.setBounds(208, 386, 44, 14);
		panel.add(lblUmbral);

		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("Calibri", 0, 11));
		btnGuardar.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {
				try {
					if ((UI.this.code.isEmpty()) || (UI.this.codigo.getText().equals(UI.this.ultimoEstado))) {
						JLabel errorSave = new JLabel("No encontr� una cadena nueva o no hay nada.");
						errorSave.setFont(new Font("Calibri", 0, 12));
						JOptionPane MsjError = new JOptionPane(errorSave);
						JDialog VentanaError = MsjError.createDialog(null, "Nada que guardar");
						VentanaError.setLocationRelativeTo(panel);
						VentanaError.setVisible(true);
					}
					else
					{
						DateFormat fecha = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
						Date date = new Date();
						PrintWriter writer = new PrintWriter(System.getProperty("user.home") + "\\Desktop\\" + "ADN_" + fecha.format(date) + ".txt", "UTF-8");
						writer.println(UI.this.codigo.getText());
						writer.close();
						UI.this.ultimoEstado = UI.this.codigo.getText();
					}
				}
				catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}

			}
		});
		btnGuardar.setBounds(388, 377, 85, 23);
		panel.add(btnGuardar);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FileDialog fd = new FileDialog((Frame)UI.this.getParent(), "Elige un archivo", 0);

				String raizUsuario = System.getProperty("user.home");
				fd.setDirectory(raizUsuario + "\\Desktop");
				fd.setVisible(true);

				UI.this.filename = (fd.getDirectory() + fd.getFile());

				if (fd.getFile() == null) {
					UI.this.directorio.setText("No se seleccion� nada");
				} else {
					UI.this.directorio.setText(UI.this.filename);
					try
					{
						UI.this.codigo.setText(UI.this.readFile(UI.this.filename));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}
}
